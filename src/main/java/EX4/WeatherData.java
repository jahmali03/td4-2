package EX4;
import java.util.*;
public class WeatherData implements Subject{


    private List <Observer> observers;
    private double temperature;
    private double humidity;
    private double pressure;

    public WeatherData() {
        this.observers = new ArrayList<>();
    }

    @Override
    public void registerObserver(Observer o) {
        observers.add(o);
    }

    public void removeObserver(Observer o){
        observers.remove(o);
    }

    public void notifyObservers(){

        System.out.println("Notify observers.");
    }

    public void setTemperature(double t){
        this.temperature = t;
    }

    public void setPressure(double p){
        this.pressure = p;
    }

    public void setHumidity(double h){
        this.humidity = h;
    }

    public void measurementChanged(){

    }

}

package EX4;

public class CurentConditionsDisplay implements Observer, DisplayMeasurments {

    private double temperature;
    private double pressure;
    private double humidity;

    public void update(double T, double P, double H){

        this.temperature = T;
        this.pressure = P;
        this.humidity = H;
    }

    public void display(){

    }

}

package EX4;

public interface Observer {

    void update(double T, double P, double H);
}

package EX3;

public abstract class Logistics {

    public abstract void planDelivery(String destination);
    public abstract Transport createTransport();

}

package EX3;

public interface Transport {

    void deliver(String destination);
}

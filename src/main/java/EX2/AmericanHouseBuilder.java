package EX2;

public class AmericanHouseBuilder extends Build{
    @Override
    public void startBuilding(){

        System.out.println("I build with "+ Material.mat +" material");
    }

    @Override
    public void setMaterial(Material mat) {
        material = mat;
    }

}

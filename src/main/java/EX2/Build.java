package EX2;

public abstract class Build {

    public Material material = new Wood();
    public abstract void startBuilding();
    public abstract void setMaterial(Material material);

}

package EX2;

public class StrategyPatternTest {

    public static void main(String [] args){
        Build builder = new AmericanHouseBuilder();
        builder.startBuilding();

        builder.setMaterial(new RedBricks());
        builder.startBuilding();
    }

}
